<?php

namespace Drupal\jee_portfolio\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "hello_block",
 *   admin_label = @Translation("Hello block"),
 * )
 */
class JeePortfolioBlock extends BlockBase implements BlockPluginInterface {

	/**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = \Drupal::config('jee_portfolio.settings');
    return array(
      'name' => $default_config->get('hello.name'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
  	$config = $this->getConfiguration();

  	if (!empty($config['name'])) {
  		$name = $config['name'];
  	} else {
  		$name = $this->t('to no one');
  	}

    return array(
      '#markup' => $this->t('Hello, @name!', array (
      		'@name' => $name,
      	)
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['jee_portfolio_block_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Who'),
      '#description' => $this->t('Who do you want to say hello to?'),
      '#default_value' => isset($config['name']) ? $config['name'] : '',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['name'] = $form_state->getValue('jee_portfolio_block_name');
  }

}
