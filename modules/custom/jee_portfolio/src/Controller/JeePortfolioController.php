<?php

namespace Drupal\jee_portfolio\Controller;

use Drupal\Core\Controller\ControllerBase;

class JeePortfolioController extends ControllerBase {

  public function content() {
	return array(
		'#type' => 'markup',
		'#markup' => $this->t('Hello World!'),
	);
  }
}
